from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField('Название', max_length=200)

    class Meta:
        verbose_name = 'Категории'
        verbose_name_plural = verbose_name

class News(models.Model):
    title = models.CharField(max_length=150)
    content = models.TextField(blank=True)
    published = models.DateTimeField()
 
    source = models.CharField(max_length=200, blank=True, null=True)
 
    rating = models.IntegerField(default=0, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=None, null=True, blank=True)

    @property
    def views_count(self):
        return History.objects.filter(news=self).values('user').distinct().count()

    def __str__(self) -> str:
        return self.title

    class Meta:
        verbose_name = 'Новости'
        verbose_name_plural = 'Новости'


class Comment(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE,  null=True, blank=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE,  null=True, blank=True)
    text = models.TextField()
    published = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'


class History(models.Model):
    user = models.CharField(max_length=100)
    dt_created = models.DateTimeField(auto_now=True)
    news = models.ForeignKey(News, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}'

    class Meta:
        verbose_name = 'История просмотров'
        verbose_name_plural = 'Истории просмотров'


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    news = models.ForeignKey(News, on_delete=models.CASCADE)


    class Meta:
        verbose_name = 'Избранное'
        verbose_name_plural = 'Избранное'