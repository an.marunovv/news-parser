from django.shortcuts import render
from .models import News, History, Favorite
from django.db import connection
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q, Count
from django.views.decorators.cache import cache_page

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def index(request):
    search = request.GET.get('search')
    most_viewed = request.GET.get('most_viewed')
    favorite = request.GET.get('favorite')
    popular = request.GET.get('popular')
    history = request.GET.get('history')
    context = {}
    if search:
        object_list = News.objects.filter(
            Q(content__contains=search) |
            Q(title__contains=search)
        )
        context['search'] = search
    elif most_viewed:
        context['filter'] = 'most_viewed'
        context['data'] = 'Самое просматриваемое'
        object_list = sorted(News.objects.all(), key=lambda news: -news.views_count)
    elif popular:
        context['data'] = 'Популярное'
        context['filter'] = 'popular'
        object_list = News.objects.all().order_by('-rating')
    elif favorite:
        context['data'] = 'Избранное'
        context['filter'] = 'favorite'
        favorite_ids = list(Favorite.objects.filter(user=request.user).values_list('news__id', flat=True))
        object_list = News.objects.filter(pk__in=favorite_ids)
    elif history:
        context['filter'] = 'history'
        context['data'] = 'История просмотров'

        history= History.objects.filter(user=request.user).order_by('-dt_created')
        object_list = []
        for h in history:
            n = News.objects.get(pk=h.news_id)
            n.saw = h.dt_created
            object_list.append(n)

    else:
        object_list = News.objects.all().order_by('-published')
    paginator = Paginator(object_list, 10)  # 3 posts in each page
    page = request.GET.get('page')
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        news = paginator.page(1)
        page = 1
    except EmptyPage:
        # If page is out of range deliver last page of results
        news = paginator.page(paginator.num_pages)
    print(page)
    context.update({'page': paginator.page(page),
                   'news': news,
                   })
    return render(request,
                  'index.html', context)



import datetime
from django.core.cache import cache
def detail(request, pk):
    if request.method == 'POST':
        comment_text = request.POST['text']
        time = datetime.datetime.now()
        Comment.objects.create(
            author=request.user,
            news_id=pk,
            text=comment_text,
            published=time
        )
        return redirect('detail', pk)


    article = News.objects.filter(pk=pk).first()
    print(article.title)
    comments = Comment.objects.filter(news=pk)
    actor = request.user
    if actor.is_anonymous:
        actor.username = get_client_ip(request)
    History.objects.create(
        user=actor.username,
        news=article
    )
    view_count = History.objects.filter(news=article).values('user', 'news').distinct().count()
    return render(
        request, 'detail.html',
          {
              'article': article,
              'comments': comments,
              'view_count': view_count,
              'favorite': request.user.favorite_set.values_list('news_id', flat=True)
          }
        )


from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth import authenticate, login, logout

from django.contrib import messages

from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import *
from .forms import CreateUserForm


def registerPage(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Аккаунт успешно создан для ' + user)

                return redirect('login')

        context = {'form': form}
        return render(request, 'accounts/register.html', context)


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('index')
            else:
                messages.info(request, 'Некорректные учетные данные')

        context = {}
        return render(request, 'accounts/login.html', context)


def logoutUser(request):
    logout(request)
    return redirect('login')


def manage_rating(request):
    news_id = request.GET.get('news_id')
    up = request.GET.get('up')
    down = request.GET.get('down')
    news = News.objects.filter(pk=news_id).first()
    if up:
        news.rating += 1
    if down:
        news.rating -= 1
    news.save()
    return redirect('detail', news_id)


from .models import Favorite
def add_to_favor(request):
    news_id = request.GET.get('news_id')

    Favorite.objects.create(user=request.user, news_id=int(news_id))
    return redirect('/', news_id)


def del_from_favor(request):
    news_id = request.GET.get('news_id')

    Favorite.objects.filter(user=request.user, news_id=int(news_id)).delete()
    return redirect('/', news_id)
