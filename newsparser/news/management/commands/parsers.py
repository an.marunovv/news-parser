from urllib import request
import re
import requests
import html2text
import datetime
from pprint import pprint
from bs4 import BeautifulSoup
month_for_rt = {
    'января' : '1',
    'февраля' : '2',
    'марта' : '3', 
    'апреля':'4',
    'мая': '5',
    'июня': '6',
    'июля': '7',
    'августа':'8',
    'сентября':'9',
    'октября':'10',
    'ноября':'11',
    'декабря':'12'
}


def RussiaTodayParser():
    news_url = 'https://russian.rt.com/news'
    root_url = 'https://russian.rt.com/'
    doc = requests.get(news_url).content.decode('utf-8')
    doc = doc.replace('\n', '')
    links = re.findall('<a class="link link_color *".*?>.*?</a>', doc)
    results = []
    # return(links)
    for link in links:
        href = link[link.find('/'):link.find('">')]
        article_title = link[link.find('>') + 1:link.rfind('<')].strip()
        doc = requests.get(root_url + href).content.decode('utf-8')
        if(re.search('read-more__summary', doc)): continue
        match_author = re.search('<div class="article__author.*?>.*?</div>', doc)
        author=None
        if match_author:
            match_author = doc[match_author.start():match_author.end()]
            author = match_author[match_author.find('>')+1:match_author.rfind('<')].strip()
        article_text = html2text.HTML2Text()
        article_text.ignore_links = True
        article_text.body_width = False
        article_text = article_text.handle(doc)
        main_content = article_text[article_text.find('Короткая ссылка'):article_text.find('Ошибка в тексте?')]
        match_img_src = re.search('\(.+?\.(png|jpg|jpeg)\)', main_content, re.IGNORECASE)
        img_src = main_content[match_img_src.start()+1:match_img_src.end()-1] if match_img_src else None
        main_content = main_content.split('\n')[2:]
        date = main_content[0].split()
        date[1] = month_for_rt[date[1]]
        date = ' '.join(date)
        date = datetime.datetime.strptime(date, '%d %m %Y, %H:%M')
        main_content =[line for line in main_content[1:] if line]
        i = 0
        index_remove =[]
        for line in main_content:
            if line.endswith('.png)') or line.endswith('.jpg)') or re.search('\*', line):
                index_remove.append(i)
            if line.startswith('#'):
                idx = main_content.index(line)
                main_content[idx] = f'<h3> {line} </h3> <br>' 
            i += 1
        for index in index_remove:
            main_content[index] = ''
        if author:
            main_content = main_content[1:]
        result = dict(
            content='\n'.join(main_content),
            published=date,
            title=article_title,
            source = root_url + href,
            author=author
        )
        results.append(result)

    return results


def RiaNewsParser():
    classes = ['Культура', 'Бизнес', 'Авто', 'Спорт', 'Общество', 'Наука', 'Технологии', 'Политика']
    news_url = 'https://ria.ru/lenta/'
    root_url = 'https://ria.ru/'
    html = requests.get(news_url).content.decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    all_links = soup.find_all('a')
    target_links = []
    for link in all_links:
        link_classes = link.get('class')
        if link_classes and link_classes[0] == 'list-item__image':
            target_links.append( (root_url + link.get('href'), link.picture.source.get('srcset')) )
    results = []
    for link, photo in target_links:
        html = requests.get(link).content.decode('utf-8')
        article_soup = BeautifulSoup(html, 'html.parser')
        main_content = article_soup.find(class_='article__text')
        title = article_soup.title.get_text()
        date_str = article_soup.find(class_='article__info-date').get_text()
        match = re.search(r'\d\d:\d\d \d\d\.\d\d\.\d\d\d\d', date_str)
        date_str = date_str[match.start():match.end()]
        date = datetime.datetime.strptime(date_str, '%H:%M %d.%m.%Y')
        result = dict(
            content=main_content.get_text(),
            published=date,
            title=title[:title.index('-')],
            source = link
            
        )
        results.append(result)
    return results


def GazetaParser():
    classes = ['Культура', 'Бизнес', 'Авто', 'Спорт', 'Общество', 'Наука', 'Технологии', 'Политика']
    news_url = 'https://vz.ru/news'
    root_url = 'https://vz.ru/'
    html = requests.get(news_url).content.decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    all_links = soup.find_all('a')
    target_links = []
    r = r'^https://vz.ru//news/\d'
    for link in all_links:
        url =  root_url + link.get('href')
        if re.search(r, url):
       
            target_links.append(url)
    results = []
    for link in target_links[:30]:
        html = requests.get(link).content.decode('utf-8')
        if html:
            article_soup = BeautifulSoup(html, 'html.parser')
            main_content = article_soup.find(class_='text newtext')
            title = article_soup.title.get_text()
            if 'Страница не найдена' in title: continue
            date = article_soup.find(class_='extra').get_text().split('\n')

            if len(date) > 1:
                date = date[1].split()
                date[1] = month_for_rt[date[1]]
                date[2] = date[2][:-1]
                date = ' '.join(date)
                date = datetime.datetime.strptime(date,'%d %m %Y %H:%M')
            else:
                date = datetime.datetime.now()

            title = title[9:-16]
            result = dict(
                content=main_content.get_text(),
                published=date,
                title=title,
                source = link
            )
            results.append(result)
    return results
