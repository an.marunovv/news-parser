from django.urls import path, include
from .views import index, detail, registerPage, loginPage, logoutUser, manage_rating, add_to_favor, del_from_favor
import debug_toolbar

urlpatterns = [
     path('', index, name='index'),
     path('detail/<int:pk>/', detail, name='detail'),
     path('register/', registerPage, name="register"),
	 path('login/', loginPage, name="login"),
	 path('logout/', logoutUser, name="logout"),
    path('manage_rating', manage_rating, name="manage_rating"),
    path('add_to_favor', add_to_favor),
    path('del_from_favor', del_from_favor)

]