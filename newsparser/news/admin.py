from django.contrib import admin
from .models import News, Comment, Category, History, Favorite

admin.site.register(News)
admin.site.register(Comment)
admin.site.register(Category)
admin.site.register(History)
admin.site.register(Favorite)
